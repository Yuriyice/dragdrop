package aizenberg.com.draganddropexample.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.Toast;

import aizenberg.com.draganddropexample.R;
import aizenberg.com.draganddropexample.TheApplication;
import aizenberg.com.draganddropexample.core.async.IOperationListener;
import aizenberg.com.draganddropexample.core.consumer.Consumer;

public class MainActivity extends Activity implements IOperationListener<Void> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TheApplication.getInstance().populateDatabase(this);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Consumer.getConsumer().onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Consumer.getConsumer().onStop();
    }

    @Override
    public void onSuccess(Void data) {
        switchFragment();
    }

    private void switchFragment() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        Fragment fragment = MainFragment.instantiate(this, MainFragment.class.getName());
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onFailure(Throwable t) {
        showToast(t.getMessage());
    }

    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
