package aizenberg.com.draganddropexample.ui;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;

import java.util.ArrayList;
import java.util.List;

import aizenberg.com.draganddropexample.R;
import aizenberg.com.draganddropexample.core.async.IOperationListener;
import aizenberg.com.draganddropexample.core.async.impl.AsyncProxy;
import aizenberg.com.draganddropexample.core.consumer.Consumer;
import aizenberg.com.draganddropexample.model.Model;
import aizenberg.com.draganddropexample.ui.adapter.ModelDraggableAdapter;

/**
 * Created by Yuriy Aizenberg
 */
public class MainFragment extends Fragment implements ModelDraggableAdapter.IMoveListener {

    private ModelDraggableAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new ModelDraggableAdapter(Color.WHITE, getResources().getColor(R.color.colorMove));
        adapter.setMoveListener(this);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        RecyclerViewDragDropManager mRecyclerViewDragDropManager = new RecyclerViewDragDropManager();
        mRecyclerViewDragDropManager.setInitiateOnLongPress(true);
        mRecyclerViewDragDropManager.setInitiateOnMove(false);

        GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
        RecyclerView.Adapter wrappedAdapter = mRecyclerViewDragDropManager.createWrappedAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(wrappedAdapter);
        recyclerView.setItemAnimator(animator);
        mRecyclerViewDragDropManager.attachRecyclerView(recyclerView);
        populateList();
    }


    private void populateList() {
        AsyncProxy.getModelsFromDB(new IOperationListener<List<Model>>() {
            @Override
            public void onSuccess(List<Model> data) {
                adapter.setData(data);
            }

            @Override
            public void onFailure(Throwable t) {
                if (getActivity() != null) {
                    MainActivity.class.cast(getActivity()).showToast(t.getMessage());
                }
            }
        });
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        reLink(fromPosition, toPosition);
        adapter.beforeDataRedraw();

    }

    private void reLink(int fromPosition, int toPosition) {
        List<Model> data = adapter.getData();
        int size = data.size();
        Model draggedModel = data.get(fromPosition);

        List<Model> consumerModels = new ArrayList<>();

        if (toPosition > fromPosition) {

            draggedModel.setLinkedTo(data.get(toPosition).getId());

            if (fromPosition == 0) {
                Model model = data.get(1);
                model.setLinkedTo(0L);
                consumerModels.add(model);
            } else {
                Model model = data.get(fromPosition + 1);
                model.setLinkedTo(data.get(fromPosition - 1).getId());
                consumerModels.add(model);
            }

            if (toPosition < size - 1) {
                Model model = data.get(toPosition + 1);
                model.setLinkedTo(draggedModel.getId());
                consumerModels.add(model);
            }

        } else {
            if (fromPosition < size - 1) {
                Model model = data.get(fromPosition + 1);
                model.setLinkedTo(data.get(fromPosition - 1).getId());
                consumerModels.add(model);
            }

            if (toPosition == 0) {
                draggedModel.setLinkedTo(0L);
                Model model = data.get(0);
                model.setLinkedTo(draggedModel.getId());
                consumerModels.add(model);
            } else {
                Model model = data.get(toPosition);
                model.setLinkedTo(draggedModel.getId());
                consumerModels.add(model);
                draggedModel.setLinkedTo(data.get(toPosition - 1).getId());
            }
        }

        consumerModels.add(draggedModel);
        Consumer.getConsumer().send(consumerModels.toArray(new Model[consumerModels.size()]));
    }
}
