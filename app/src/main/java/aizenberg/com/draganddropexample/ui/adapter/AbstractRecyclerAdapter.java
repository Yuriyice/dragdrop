package aizenberg.com.draganddropexample.ui.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import aizenberg.com.draganddropexample.model.AbstractModel;

/**
 * Created by Yuriy Aizenberg
 */
public abstract class AbstractRecyclerAdapter<T extends AbstractModel, V extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<V> {

    private final List<T> data = new ArrayList<>();

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data, boolean clear) {
        if (clear) {
            this.data.clear();
        }
        this.data.addAll(data);
        beforeDataRedraw();
        notifyDataSetChanged();
    }

    protected void beforeDataRedraw() {

    }

    public void setData(List<T> data) {
        setData(data, true);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public T getObject(int position) {
        return data.get(position);
    }
}
