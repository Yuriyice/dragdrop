package aizenberg.com.draganddropexample.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;

import java.util.List;

import aizenberg.com.draganddropexample.R;
import aizenberg.com.draganddropexample.model.Model;
import aizenberg.com.draganddropexample.utils.ModelSorter;

/**
 * Created by Yuriy Aizenberg
 */
public class ModelDraggableAdapter extends AbstractRecyclerAdapter<Model, ModelDraggableAdapter.ViewHolder> implements DraggableItemAdapter<ModelDraggableAdapter.ViewHolder> {


    private IMoveListener moveListener;
    private final int defaultColor;
    private final int dragColor;

    public void setMoveListener(IMoveListener moveListener) {
        this.moveListener = moveListener;
    }

    public ModelDraggableAdapter(int defaultColor, int dragColor) {
        this.defaultColor = defaultColor;
        this.dragColor = dragColor;
        setHasStableIds(true);
    }

    private interface Draggable extends DraggableItemConstants {
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate((viewType == 0) ? R.layout.model_list_item_default : R.layout.model_list_item_dragged, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public long getItemId(int position) {
        return getObject(position).getId();
    }

    @Override
    public void beforeDataRedraw() {
        super.beforeDataRedraw();
        List<Model> models = getData();
        List<Model> structureTree = ModelSorter.newSorter(models).structureTree();

        models.clear();
        models.addAll(structureTree);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Model object = getObject(position);
        holder.txtDescription.setText(object.getDescription());
        holder.txtTitle.setText(object.getTitle());

        final int dragState = holder.getDragStateFlags();

        if (((dragState & Draggable.STATE_FLAG_IS_UPDATED) != 0)) {
            int bgResId;

            if ((dragState & Draggable.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = dragColor;
            } else if ((dragState & Draggable.STATE_FLAG_DRAGGING) != 0) {
                bgResId = dragColor;
            } else {
                bgResId = defaultColor;
            }

            holder.rootView.setBackgroundColor(bgResId);
        }
    }

    @Override
    public boolean onCheckCanStartDrag(ViewHolder holder, int position, int x, int y) {
        return true;
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(ViewHolder holder, int position) {
        return null;
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        if (moveListener != null) {
            moveListener.onMoveItem(fromPosition, toPosition);

        }
        notifyItemMoved(fromPosition, toPosition);
    }

    static class ViewHolder extends AbstractDraggableItemViewHolder {

        private ViewGroup rootView;
        private TextView txtTitle;
        private TextView txtDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            rootView = (ViewGroup) itemView.findViewById(R.id.list_item_root);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_list_item_title);
            txtDescription = (TextView) itemView.findViewById(R.id.txt_list_item_description);
        }
    }

    public interface IMoveListener {
        void onMoveItem(int fromPosition, int toPosition);
    }
}
