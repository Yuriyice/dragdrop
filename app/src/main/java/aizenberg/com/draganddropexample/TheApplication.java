package aizenberg.com.draganddropexample;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import aizenberg.com.draganddropexample.core.async.AbstractNullableAsyncOperation;
import aizenberg.com.draganddropexample.core.async.IOperationListener;
import aizenberg.com.draganddropexample.db.dao.crud.ModelCursorCrud;
import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class TheApplication extends Application {

    private static TheApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static TheApplication getInstance() {
        return instance;
    }

    public void populateDatabase(IOperationListener<Void> listener) {
        new AbstractNullableAsyncOperation<Void>(listener) {
            @Override
            protected Void run() throws Throwable {
                ModelCursorCrud crud = new ModelCursorCrud();
                if (crud.getList().isEmpty()) {
                    List<Model> models = new ArrayList<>();
                    for (int i = 0; i < 20; i++) {
                        models.add(new Model("Item: " + (i + 1), "Awesome list item " + (i + 1), (i == 0) ? null : (long) i));
                    }
                    crud.batchSave(models.toArray(new Model[models.size()]));
                }
                return null;
            }
        }.execute();
    }


}
