package aizenberg.com.draganddropexample.db.dao;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import aizenberg.com.draganddropexample.db.core.TableDataHolder;
import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class ModelDataConverter implements IDataAccessConverter<Model> {

    @Override
    public Model byId(Cursor cursor) {
        Model model = null;
        if (cursor != null && cursor.moveToFirst()) {
            model = parseModelInternal(cursor);
        }
        closeCursor(cursor);
        return model;
    }

    private Model parseModelInternal(Cursor cursor) {
        Model model;
        model = new Model();
        model.setId(cursor.getLong(cursor.getColumnIndex(TableDataHolder.MODEL_KEY_ID)));
        model.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TableDataHolder.MODEL_KEY_TITLE)));
        model.setDescription(cursor.getString(cursor.getColumnIndexOrThrow(TableDataHolder.MODEL_KEY_DESCRIPTION)));
        model.setLinkedTo(cursor.getLong(cursor.getColumnIndex(TableDataHolder.MODEL_KEY_LINKED_TO)));
        return model;
    }

    @Override
    public List<Model> getList(Cursor cursor) {
        List<Model> list = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(parseModelInternal(cursor));
            } while (cursor.moveToNext());
        }
        closeCursor(cursor);
        return list;
    }

    @Override
    public ContentValues update(Model data) {
        ContentValues cv = new ContentValues();
        cv.put(TableDataHolder.MODEL_KEY_LINKED_TO, data.getLinkedTo());
        return cv;
    }

    @Override
    public ContentValues create(Model data) {
        ContentValues cv = new ContentValues();
        cv.put(TableDataHolder.MODEL_KEY_TITLE, data.getTitle());
        cv.put(TableDataHolder.MODEL_KEY_DESCRIPTION, data.getDescription());
        cv.put(TableDataHolder.MODEL_KEY_LINKED_TO, data.getLinkedTo());
        return cv;
    }

    private void closeCursor(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
}
