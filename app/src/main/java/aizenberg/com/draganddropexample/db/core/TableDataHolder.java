package aizenberg.com.draganddropexample.db.core;

/**
 * Created by Yuriy Aizenberg
 */
public class TableDataHolder {

    public static final String MODEL_TABLE_NAME = "model";
    public static final String MODEL_KEY_ID = "id";
    public static final String MODEL_KEY_TITLE = "title";
    public static final String MODEL_KEY_DESCRIPTION = "description";
    public static final String MODEL_KEY_LINKED_TO = "linked_to";

}
