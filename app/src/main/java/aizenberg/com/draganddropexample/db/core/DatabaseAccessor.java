package aizenberg.com.draganddropexample.db.core;

/**
 * Created by Yuriy Aizenberg
 */
public class DatabaseAccessor {

    private static DatabaseAccessor instance;
    private static final Object MUTEX = new Object();
    private DatabaseHelper helper;

    private DatabaseAccessor() {
        helper = new DatabaseHelper();
    }

    public static DatabaseAccessor getInstance() {
        if (instance == null) {
            synchronized (MUTEX) {
                if (instance == null) {
                    instance = new DatabaseAccessor();
                }
            }
        }
        return instance;
    }


    public DatabaseHelper getHelper() {
        return helper;
    }
}
