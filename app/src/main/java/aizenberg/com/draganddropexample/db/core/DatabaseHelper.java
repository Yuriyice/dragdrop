package aizenberg.com.draganddropexample.db.core;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import aizenberg.com.draganddropexample.TheApplication;

/**
 * Created by Yuriy Aizenberg
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;
    public static final String NAME = "drag_and_drop";


    public DatabaseHelper() {
        super(TheApplication.getInstance(), NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlPattern = "CREATE TABLE IF NOT EXISTS %s " +
                "(%s integer primary key autoincrement, " +
                "%s text not null, " +
                "%s text not null, " +
                "%s integer);";

        db.execSQL(String.format(sqlPattern,
                TableDataHolder.MODEL_TABLE_NAME,
                TableDataHolder.MODEL_KEY_ID,
                TableDataHolder.MODEL_KEY_TITLE,
                TableDataHolder.MODEL_KEY_DESCRIPTION,
                TableDataHolder.MODEL_KEY_LINKED_TO));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



}
