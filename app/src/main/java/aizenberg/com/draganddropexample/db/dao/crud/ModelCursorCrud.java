package aizenberg.com.draganddropexample.db.dao.crud;

import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import aizenberg.com.draganddropexample.db.core.DatabaseAccessor;
import aizenberg.com.draganddropexample.db.core.DatabaseHelper;
import aizenberg.com.draganddropexample.db.core.TableDataHolder;
import aizenberg.com.draganddropexample.db.dao.IDataAccessConverter;
import aizenberg.com.draganddropexample.db.dao.ModelDataConverter;
import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class ModelCursorCrud implements CursorCrud<Model, Long> {

    private IDataAccessConverter<Model> converter;
    private DatabaseHelper helper;


    public ModelCursorCrud() {
        converter = new ModelDataConverter();
        helper = DatabaseAccessor.getInstance().getHelper();
    }

    @Override
    public Model byId(Long aLong) {
        String whereClause = TableDataHolder.MODEL_KEY_ID + " = " + aLong;
        return converter.byId(helper
                .getWritableDatabase()
                .query(TableDataHolder.MODEL_TABLE_NAME, null, whereClause, null, null, null, null));
    }

    @Override
    public List<Model> getList() {
        return converter.getList(helper
                .getWritableDatabase()
                .query(TableDataHolder.MODEL_TABLE_NAME, null, null, null, null, null, null));

    }

    @Override
    public Long save(Model object) {
        return saveInternal(object, helper.getWritableDatabase());
    }

    private long saveInternal(Model object, SQLiteDatabase database) {
        return database.insert(TableDataHolder.MODEL_TABLE_NAME, null, converter.create(object));
    }

    @Override
    public int update(Model object) {
        return updateInternal(object, helper.getWritableDatabase());
    }

    private int updateInternal(Model object, SQLiteDatabase database) {
        String whereClause = TableDataHolder.MODEL_KEY_ID + " = ?";
        return database.update(TableDataHolder.MODEL_TABLE_NAME, converter.update(object), whereClause, new String[]{String.valueOf(object.getId())});
    }

    @Override
    public int delete(Model object) {
        String whereClause = TableDataHolder.MODEL_KEY_ID + " = ?";
        return helper.getWritableDatabase().delete(TableDataHolder.MODEL_TABLE_NAME, whereClause, new String[]{String.valueOf(object.getId())});
    }

    @Override
    public void batchUpdate(Model... objects) {
        if (objects != null && objects.length > 0) {
            SQLiteDatabase writableDatabase = helper.getWritableDatabase();
            writableDatabase.beginTransaction();
            try {
                for (Model model : objects) {
                    updateInternal(model, writableDatabase);
                }
                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        }
    }

    @Override
    public void batchSave(Model... objects) {
        if (objects != null && objects.length > 0) {
            SQLiteDatabase writableDatabase = helper.getWritableDatabase();
            writableDatabase.beginTransaction();
            try {
                for (Model model : objects) {
                    saveInternal(model, writableDatabase);
                }
                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
            }
        }
    }


}
