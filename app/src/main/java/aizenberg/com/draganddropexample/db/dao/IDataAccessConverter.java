package aizenberg.com.draganddropexample.db.dao;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.List;

/**
 * Created by Yuriy Aizenberg
 */
public interface IDataAccessConverter<T> {

    T byId(Cursor cursor);

    List<T> getList(Cursor cursor);

    ContentValues update(T data);

    ContentValues create(T data);

}
