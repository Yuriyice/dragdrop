package aizenberg.com.draganddropexample.db.dao.crud;

import java.util.List;

/**
 * Created by Yuriy Aizenberg
 */
public interface CursorCrud<T, ID> {

    T byId(ID id);

    List<T> getList();

    Long save(T object);

    int update(T object);

    int delete(T object);


    void batchUpdate(T... objects);


    void batchSave(T... objects);
}

