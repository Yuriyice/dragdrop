package aizenberg.com.draganddropexample.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class ModelSorter {

    private int dataSize;
    private Map<Long, Model> linkedObjects = new HashMap<>();



    public static ModelSorter newSorter(List<Model> data) {
        ModelSorter sorter = new ModelSorter();
        sorter.dataSize = data.size();
        for (Model model : data) {
            sorter.linkedObjects.put(model.getLinkedTo(), model);
        }
        return sorter;
    }

    public List<Model> structureTree() {
        List<Model> models = new ArrayList<>();
        Model firstModel = find(0L);
        models.add(0, firstModel);
        while (models.size() < dataSize) {
            firstModel = find(firstModel.getId());
            models.add(firstModel);

        }
        return models;
    }

    private Model find(Long likedTo) {
        return linkedObjects.get(likedTo);
    }




}
