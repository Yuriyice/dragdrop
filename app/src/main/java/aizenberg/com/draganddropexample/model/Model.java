package aizenberg.com.draganddropexample.model;

import aizenberg.com.draganddropexample.core.consumer.ISignal;

/**
 * Created by Yuriy Aizenberg
 */
public class Model extends AbstractModel implements ISignal {

    private String title;
    private String description;
    private Long linkedTo;

    public Model() {
    }

    public Model(String title, String description, Long linkedTo) {
        this.title = title;
        this.description = description;
        this.linkedTo = linkedTo;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLinkedTo() {
        return linkedTo;
    }

    public void setLinkedTo(Long linkedTo) {
        this.linkedTo = linkedTo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Model model = (Model) o;

        if (title != null ? !title.equals(model.title) : model.title != null) return false;
        if (description != null ? !description.equals(model.description) : model.description != null)
            return false;
        return linkedTo != null ? linkedTo.equals(model.linkedTo) : model.linkedTo == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (linkedTo != null ? linkedTo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Model{" +
                "id='" + getId() + '\'' +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", linkedTo=" + linkedTo +
                '}';
    }

}
