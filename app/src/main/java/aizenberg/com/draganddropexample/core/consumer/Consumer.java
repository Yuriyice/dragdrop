package aizenberg.com.draganddropexample.core.consumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class Consumer implements LifecycleHook {

    private static Consumer consumer;

    private ConsumerWorker workerInstance;
    private ExecutorService executorService;
    private final BlockingQueue<ISignal> sharedQueue = new LinkedBlockingQueue<>();
    private boolean stopped;

    public static Consumer getConsumer() {
        if (consumer == null) {
            consumer = new Consumer();
        }
        return consumer;
    }

    private Consumer() {
    }

    @Override
    public void onStart() {
        executorService = Executors.newSingleThreadExecutor();
        workerInstance = new ConsumerWorker(sharedQueue);
        executorService.execute(workerInstance);
        stopped = false;
    }

    @Override
    public void onStop() {
        if (executorService != null) {
            send(new ShutdownSignal());
            executorService.shutdown();
            stopped = true;
            executorService = null;
            workerInstance = null;
        }
    }

    public void send(ISignal... signals) {
        if (stopped) throw new IllegalStateException("Consumer already stopped");
        for (ISignal signal : signals) {
            try {
                if (signal == null) continue;
                sharedQueue.put(signal);
            } catch (InterruptedException ignored) {

            }
        }
    }
}
