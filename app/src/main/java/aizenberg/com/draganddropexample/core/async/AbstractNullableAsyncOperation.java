package aizenberg.com.draganddropexample.core.async;

/**
 * Created by Yuriy Aizenberg
 */
public abstract class AbstractNullableAsyncOperation<T> extends AbstractAsyncOperation<T> {


    public AbstractNullableAsyncOperation(IOperationListener<T> listener) {
        super(listener);
    }

    @Override
    protected boolean isNullAcceptableInResult() {
        return true;
    }
}
