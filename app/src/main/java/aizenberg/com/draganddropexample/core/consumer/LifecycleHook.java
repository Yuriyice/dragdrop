package aizenberg.com.draganddropexample.core.consumer;

/**
 * Created by Yuriy Aizenberg
 */
public interface LifecycleHook {

    void onStart();

    void onStop();

}
