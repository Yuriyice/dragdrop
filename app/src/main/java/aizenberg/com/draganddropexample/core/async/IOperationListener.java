package aizenberg.com.draganddropexample.core.async;

/**
 * Created by Yuriy Aizenberg
 */
public interface IOperationListener<T> {

    void onSuccess(T data);

    void onFailure(Throwable t);


}
