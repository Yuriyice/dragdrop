package aizenberg.com.draganddropexample.core.consumer;

import android.util.Log;

import java.util.concurrent.BlockingQueue;

import aizenberg.com.draganddropexample.db.dao.crud.ModelCursorCrud;
import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class ConsumerWorker implements Runnable {

    private static final String TAG = ConsumerWorker.class.getSimpleName();
    private BlockingQueue<ISignal> sharedQueue;
    private final ModelCursorCrud crud;

    public ConsumerWorker(BlockingQueue<ISignal> sharedQueue) {
        this.sharedQueue = sharedQueue;
        crud = new ModelCursorCrud();
    }

    @Override
    public void run() {
        try {
            while (true) {
                ISignal signal = sharedQueue.take();
                if (signal == null) continue;

                if (signal instanceof Model) {
                    crud.update((Model) signal);
                } else if (signal instanceof ShutdownSignal) {
                    break;
                } else {
                    throw new IllegalArgumentException(String.format("Signal %s not recognized", signal.getClass().getName()));
                }
            }
        } catch (InterruptedException e) {
            Log.d(TAG, e.getMessage(), e);
        }
    }

}
