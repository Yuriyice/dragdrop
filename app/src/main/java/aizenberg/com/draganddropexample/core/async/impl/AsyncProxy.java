package aizenberg.com.draganddropexample.core.async.impl;

import java.util.List;

import aizenberg.com.draganddropexample.core.async.AbstractAsyncOperation;
import aizenberg.com.draganddropexample.core.async.IOperationListener;
import aizenberg.com.draganddropexample.db.dao.crud.ModelCursorCrud;
import aizenberg.com.draganddropexample.model.Model;

/**
 * Created by Yuriy Aizenberg
 */
public class AsyncProxy {

    public static void getModelsFromDB(IOperationListener<List<Model>> listIOperationListener) {
        new AbstractAsyncOperation<List<Model>>(listIOperationListener) {
            @Override
            protected List<Model> run() throws Throwable {
                return new ModelCursorCrud().getList();
            }
        }.execute();
    }


}
