package aizenberg.com.draganddropexample.core.async;

import android.os.AsyncTask;

/**
 * Created by Yuriy Aizenberg
 */
public abstract class AbstractAsyncOperation<T> extends AsyncTask<Void, Void, T> {

    private IOperationListener<T> listener;
    private Throwable throwable;

    public AbstractAsyncOperation(IOperationListener<T> listener) {
        this.listener = listener;
    }

    protected abstract T run() throws Throwable;

    @Override
    protected final T doInBackground(Void... params) {
        try {
            return run();
        } catch (Throwable throwable) {
            this.throwable = throwable;
        }
        return null;
    }

    protected boolean isNullAcceptableInResult() {
        return false;
    }

    @Override
    protected void onPostExecute(T t) {
        super.onPostExecute(t);
        if (listener != null) {
            if (t != null || (throwable == null && isNullAcceptableInResult())) {
                listener.onSuccess(t);
            } else if (throwable != null) {
                listener.onFailure(throwable);
            } else {
                listener.onFailure(new RuntimeException("No objects in result?"));
            }
        }
    }
}
